from django import forms
from snowpenguin.django.recaptcha2.fields import ReCaptchaField
from snowpenguin.django.recaptcha2.widgets import ReCaptchaWidget
from posts.models import Post, UserProfile, Comment
from django.contrib.auth.models import User


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = [
            "category",
            "title",
            "slug",
            "content",
            "image",
            "keywords",
            "description",
        ]


class ContactForm(forms.Form):
    subject = forms.CharField(max_length=100)
    name = forms.CharField(required=True, max_length=100)
    sender = forms.EmailField(required=True, max_length=100)
    message = forms.CharField(required=True, max_length=1000)
    captcha = ReCaptchaField(widget=ReCaptchaWidget())


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class UserNameForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username',)


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['picture']


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        exclude = ['post', 'user', 'moderation']
        widgets = {
            'content': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Напиши хороший комментарий или ничего...',
                'rows': '6',
                'cols': '0'
            }),
        }
