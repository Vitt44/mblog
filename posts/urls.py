from django.conf.urls import url
from django.contrib.auth import views as auth_views

from .views import (post_list, post_create, post_detail, post_update, post_delete, show_category, contacts, register,
                    user_login, user_logout, like_post, user_page, change_password, user_change, contact_form)

urlpatterns = [
    url(r'^$', post_list, name="list"),
    url(r'^contact-form/$', contact_form),
    url(r'^create/$', post_create, name="create"),
    url(r'^contacts/$', contacts, name="contacts"),
    url(r'^registration/$', register, name="regist"),
    url(r'^login/$', user_login, name='login'),
    url(r'^logout/$', user_logout, name='logout'),
    url(r'^like/$', like_post, name='like_post'),
    url(r'^user/$', user_page, name='user_page'),
    url(r'^user/pw-change/$', change_password, name='change_password'),
    url(r'^user/user-change/$', user_change, name='user_change'),
    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^(?P<category>[-\w]+)/(?P<slug>[-\w]+)/$', post_detail, name="detail"),
    url(r'^(?P<category>[-\w]+)/(?P<slug>[-\w]+)/up/$', post_update, name="update"),
    url(r'^(?P<category>[-\w]+)/(?P<slug>[-\w]+)/del/$', post_delete, name="delete"),
    url(r'^(?P<slug>[-\w]+)/$', show_category, name='show_category'),

]
