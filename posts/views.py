### -*- coding: utf-8 -*- ###
import json

from urllib.parse import quote_plus
from django.contrib import messages
from django.contrib.auth import authenticate, logout, login, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm, SetPasswordForm
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, Http404, HttpResponse, JsonResponse
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
from django.core.mail import send_mail, BadHeaderError
from django.views.decorators.csrf import csrf_exempt
from posts.form import UserForm, UserProfileForm, UserNameForm
from django.core.urlresolvers import reverse
from django.contrib.sitemaps import Sitemap
from datetime import datetime

from .form import PostForm, ContactForm, CommentForm
from .models import Post, Category, Comment
from django.contrib.auth.models import User


@csrf_exempt
def contact_form(request):
    if request.method == 'POST':
        json_data = request.body.decode('utf-8')
        data = json.loads(json_data)
        # print(data)
        subject = data['theme']
        name = data['name']
        sender = data['email']
        message = data['text']
        from_email = 'vitalii-kuz@yandex.ru'
        to_email = [from_email, ]
        full_message = "От %s %s : %s " % (name, sender, message)
        status = ''
        try:
            send_mail(subject, full_message, from_email, to_email)
            status = 'success'
        except BadHeaderError:
            status = 'error'
        return JsonResponse({'status': status}, content_type='application/json')
    else:
        raise Http404


def post_list(request):
    queryset_list = Post.objects.all()
    category_list = Category.objects.all()
    search = request.GET.get("s")
    if search:
        queryset_list = queryset_list.filter(
            Q(title__icontains=search) |
            Q(content__icontains=search) #|
            #Q(user__first_name__icontains=search) |
            #Q(user__last_name__icontains=search)
        ).distinct()
    paginator = Paginator(queryset_list, 3)  # Show 5 contacts per page
    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)
    context = {
        "object_list": queryset,
        "category_list": category_list,
    }
    return render(request, "post_list.html", context)


def show_category(request, slug=None):
    category_show = {}
    try:
        category = Category.objects.get(slug=slug)
        pages = Post.objects.filter(category=category)
        category_list = Category.objects.all()
        category_show['pages'] = pages
        category_show['category'] = category
        category_show['category_list'] = category_list
    except Category.DoesNotExist:
        raise Http404
    return render(request, 'category.html', category_show)


@login_required
def post_create(request):
    if not request.user.is_authenticated():
        raise Http404
    form = PostForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.user = request.user
        instance.save()
        messages.success(request, "Created")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "form": form,
    }
    return render(request, "post_form.html", context)


def post_detail(request, slug=None, category=None):
    instance = get_object_or_404(Post, slug=slug, category__slug=category)
    comments = Comment.objects.filter(post=instance, moderation=True)
    form = CommentForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            new_comment = form.save(commit=False)
            new_comment.user = request.user
            new_comment.post = instance
            new_comment.parent = form.cleaned_data['parent']
            new_comment.content = form.cleaned_data['content']
            if new_comment.user.is_superuser:
                new_comment.moderation = True
            else:
                from_email = 'vitalii-kuz@yandex.ru'
                to_email = [from_email]
                full_message = "Комментарий от %s : %s " % (new_comment.user, new_comment.content)
                send_mail("Новый комментарий", full_message, from_email, to_email)
                messages.success(request, "Сообщение отправлено на модерацию")
            new_comment.save()
            return HttpResponseRedirect(instance.get_absolute_url())
    share_string = quote_plus(instance.title)
    context = {
        "instance": instance,
        "title": instance.title,
        "share_string": share_string,
        "nodes": comments,
        "comment_form": form,
    }
    return render(request, "post_detail.html", context)


@login_required
def post_update(request, slug=None, category=None):
    if not request.user.is_authenticated():
        raise Http404
    instance = get_object_or_404(Post, slug=slug, category__slug=category)
    form = PostForm(request.POST or None, request.FILES or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "Updated!")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "instance": instance,
        "title": instance.title,
        "form": form,
    }
    return render(request, "post_form.html", context)


def post_delete(request, slug=None, category=None):
    if not request.user.is_superuser:
        raise Http404
    instance = get_object_or_404(Post, slug=slug, category__slug=category)
    instance.delete()
    messages.success(request, "Deleted!")
    return redirect(list)


def contacts(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            name = form.cleaned_data['name']
            sender = form.cleaned_data['sender']
            message = form.cleaned_data['message']
            from_email = 'vitalii-kuz@yandex.ru'
            to_email = [from_email, ]
            full_message = "От %s %s : %s " % (name, sender, message)
            try:
                send_mail(subject, full_message, from_email, to_email)
            except BadHeaderError:
                messages.error(request, 'Инвалид что-ли!?')
            messages.success(request, "Отправлено!")
            return render(request, 'contact.html')
    else:
        form = ContactForm()
    return render(request, 'contact.html', {'form': form})


def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']
            profile.save()
            registered = True
            user_login(request)
        else:
            messages.warning(request, user_form.errors, profile_form.errors)
            print(user_form.errors, profile_form.errors)
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()
    return render(request,
                  'regist.html',
                  {'user_form': user_form,
                   'profile_form': profile_form,
                   'registered': registered})


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('list'))
            else:
                messages.warning(request, "Ваш аккаун не доступен")
                return render(request, 'login.html')
        else:
            print("Не верные данные для входа: {0}, {1}".format(username, password))
            messages.warning(request, "Неверные данные для входа.")
            return render(request, 'login.html')
    else:
        return render(request, 'login.html', {})


@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)
    # Take the user back to the homepage.
    return HttpResponseRedirect(reverse('list'))


class PostSitemap(Sitemap):
    changefreq = "weekly"
    priority = 1.0

    def items(self):
        return Post.objects.all()

    def lastmod(self, obj):
        return obj.timestamp


class StaticViewSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.7

    def items(self):
        return ['contacts', 'list']

    def location(self, item):
        return reverse(item)


def like_post(request):
    post_id = None
    if request.method == 'GET':
        post_id = request.GET['obj_id']
    if post_id:
        post = Post.objects.get(id=int(post_id))
        likes = post.likes
    if post and ('pause' not in request.session):
        request.session
        request.session['pause'] = True
        likes = post.likes + 1
        post.likes = likes
        post.save()
    return HttpResponse(likes)


@login_required
def user_page(request):
    user = request.user
    if request.method == "POST":
        pro_form = UserProfileForm(data=request.POST, instance=user.userprofile)
        if pro_form.is_valid():
            profile = pro_form.save(commit=False)
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']
            profile.save()
            update_session_auth_hash(request, pro_form.instance)
            return HttpResponseRedirect('/user/')
        else:
            messages.warning(request, pro_form.errors)
    else:
        pro_form = UserProfileForm()

    context = {
        'img_form': pro_form,
    }
    return render(request, 'account.html', context)


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        new_pass_form = SetPasswordForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Ваш пароль успешно обновлен!')
            return redirect('change_password')

        if new_pass_form.is_valid():
            user = new_pass_form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Ваш пароль успешно обновлен!')
            return redirect('change_password')
    else:
        form = PasswordChangeForm(request.user)
        new_pass_form = SetPasswordForm(request.user)
    return render(request, 'pw_change.html', {'form': form, 'new_pass_form': new_pass_form})


@login_required
def user_change(request):
    instance = User.objects.get(username=request.user.username)
    form = UserNameForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        update_session_auth_hash(request, instance)  # Important!
        messages.success(request, 'Ваш данные обновлены!')
        return redirect('user_change')
    return render(request, 'user_change.html', {'form': form})
