from rest_framework.serializers import ModelSerializer, HyperlinkedIdentityField, SerializerMethodField

from posts.models import Post


class PostCreateSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = ('category', 'title', 'slug', 'content', 'timestamp')


class PostSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = ('id', 'category', 'title', 'slug', 'content', 'timestamp')


class PostListSerializer(ModelSerializer):
    url = HyperlinkedIdentityField(view_name='posts-api:detail', lookup_field='id')
    user = SerializerMethodField()
    image = SerializerMethodField()
    category = SerializerMethodField()

    class Meta:
        model = Post
        fields = ('url', 'id', 'user', 'image', 'category', 'title', 'content', 'timestamp')

    @staticmethod
    def get_user(object):
        return str(object.user.username)

    @staticmethod
    def get_category(object):
        return str(object.category.name)

    @staticmethod
    def get_image(object):
        try:
            image = object.image.url
        except:
            image = None
        return image
