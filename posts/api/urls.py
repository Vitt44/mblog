from django.conf.urls import url

from .views import (PostListAPIView, PostDetailAPIView, PostUpdateAPIView, PostDeleteAPIView, PostCreateAPIView)

urlpatterns = [
    url(r'^$', PostListAPIView.as_view(), name="list"),
    url(r'^create/$', PostCreateAPIView.as_view(), name="create"),
    # url(r'^contacts/$', contacts, name="contacts"),
    # url(r'^login/$', user_login, name='login'),
    # url(r'^logout/$', user_logout, name='logout'),
    # url(r'^like/$', like_post, name='like_post'),
    url(r'^(?P<id>[-\w]+)/$', PostDetailAPIView.as_view(), name="detail"),
    url(r'^(?P<slug>[-\w]+)/up/$', PostUpdateAPIView.as_view(), name="update"),
    url(r'^(?P<slug>[-\w]+)/del/$', PostDeleteAPIView.as_view(), name="delete"),
    # url(r'^(?P<slug>[-\w]+)/$', show_category, name='show_category'),

]
