### -*- coding: utf-8 -*- ###
from __future__ import unicode_literals
from django.urls import reverse
from django.conf import settings
from django.contrib.auth.models import User
from redactor.fields import RedactorField
from mptt.models import MPTTModel, TreeForeignKey
from django.db import models
from django.utils.text import slugify


class Category(models.Model):
    name = models.CharField(max_length=128, unique=True, verbose_name='Название')
    slug = models.SlugField(unique=True)
    image = models.ImageField(blank=True, verbose_name='Изображение')

    class Meta:
        verbose_name = 'Категории'
        verbose_name_plural = 'Категории'

    # For Python 2, use __unicode__ too
    def __str__(self):
        return self.slug

    def get_absolute_url(self):
        return reverse('show_category', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        try:
            this_record = Category.objects.get(id=self.id)
            if this_record.image != self.image:
                this_record.image.delete(save=False)
        except:
            pass
        super(Category, self).save(*args, **kwargs)


class Post(models.Model):
    category = models.ForeignKey(Category, default=1, verbose_name='Категория')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, verbose_name='Пользователь')
    title = models.CharField(max_length=120, verbose_name='Заголовок')
    slug = models.SlugField(unique=True)
    image = models.ImageField(blank=True, verbose_name='Изображение')
    content = RedactorField(max_length=10000, verbose_name='Контент')
    timestamp = models.DateTimeField(verbose_name='Дата')
    keywords = models.CharField(max_length=1024, blank=True)
    description = models.CharField(max_length=1024, blank=True)
    likes = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'Статьи'
        verbose_name_plural = 'Статьи'
        ordering = ["-timestamp"]

    # For Python 2, use __unicode__ too
    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('detail', kwargs={'slug': self.slug, 'category': self.category})

    def save(self, *args, **kwargs):
        try:
            this_record = Post.objects.get(id=self.id)
            if this_record.image != self.image:
                this_record.image.delete(save=False)
        except:
            pass
        super(Post, self).save(*args, **kwargs)


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    picture = models.ImageField(upload_to='profile_images', blank=True)

    def __str__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        try:
            this_record = UserProfile.objects.get(id=self.id)
            if this_record.picture != self.picture:
                this_record.picture.delete(save=False)
        except:
            pass
        super(UserProfile, self).save(*args, **kwargs)


class Comment(MPTTModel):
    post = models.ForeignKey(Post)
    date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    content = models.TextField(max_length=250, unique=True)
    moderation = models.BooleanField(default=False)

    class MPTTMeta:
        order_insertion_by = ['date']

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
