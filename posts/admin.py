from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import Post, Category, Comment, UserProfile


class PostAdmin(admin.ModelAdmin):
    list_display = ["title", "category", "timestamp", "keywords", "description"]
    list_display_links = ["title"]
    list_filter = ('timestamp',)
    date_hierarchy = 'timestamp'
    ordering = ('-timestamp',)
    search_fields = ["title", "content"]
    prepopulated_fields = {'slug': ('title',)}

    class Meta:
        model = Post


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


class CommentMPTTModelAdmin(MPTTModelAdmin):
    # specify pixel amount for this ModelAdmin only:
    mptt_level_indent = 20
    list_display = ["post", "moderation", "date", "user", "parent"]
    list_display_links = ["date"]
    list_editable = ["moderation"]
    list_filter = ('date',)

    class Meta:
        model = Comment


class UserInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'Доп. поля'


class UserAdmin(UserAdmin):
    inlines = (UserInline,)


# Перерегистрируем модель User
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(Post, PostAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Comment, CommentMPTTModelAdmin)
